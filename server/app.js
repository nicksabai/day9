/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));


// app.get("/students", function(req, res) {
//     console.log("students");
//     var person1 = {
//         name: "Kenneth",
//         age: 35
//     };
//     var person2 = {
//         name: "Alvin",
//         age: 40
//     };
//     var users = [person1, person2, person1, person1];
//     res.send("My name is " + JSON.stringify(users[0]));
// });

// app.get("/users", function(req, res) {
//     console.log("users");
//     var person1 = {
//         name: "Kenneth",
//         age: 35
//     };
//     var person2 = {
//         name: "Alvin",
//         age: 40
//     };
//     var users = [person1, person2, person1, person1];
//     res.json(users);
// });

// app.post("/users", function(req, res) {
//     console.log("Received user object " + req.body);
//     console.log("Received user object " + JSON.stringify(req.body));
//     var user = req.body;
//     user.email = "hi " + user.email;
//     user.fullname = user.fullname;
//     user.gender = user.gender;
//     user.dateOfBirth = user.dateOfBirth;
//     console.log("email > " + user.email);
//     console.log("password > " + user.password);
//     console.log("fullname > " + user.fullname);
//     console.log("gender > " + user.gender);
//     console.log("D.O.B > " + user.dateOfBirth);
//     res.status(200).json(user);
// });

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});